#!/bin/bash

if [ "${*}" == "./bin/scim_client" ]; then
  amber db migrate
fi
exec "${@}"
