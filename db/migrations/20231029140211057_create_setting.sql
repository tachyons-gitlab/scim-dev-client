-- +micrate Up
CREATE TABLE settings (
  id INTEGER NOT NULL PRIMARY KEY,
  scim_token VARCHAR,
  scim_endpoint VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS settings;
