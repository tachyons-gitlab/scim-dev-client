class SettingController < ApplicationController
  def show
    settings = Setting.first || Setting.new
    render("show.slang")
  end

  def update
    settings = Setting.first || Setting.new
    settings.set_attributes settings_params.validate!
    if settings.save
      redirect_to "/"
    else
      flash[:danger] = "Could not update Settings!"
      render "show.slang"
    end
  end

  private def settings_params
    params.validation do
      required :scim_token
      required :scim_endpoint
    end
  end
end
