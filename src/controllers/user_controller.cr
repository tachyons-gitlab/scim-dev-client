require "http/client"
require "json"

class UserController < ApplicationController
  getter user = User.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_user }
  end

  def show
    render("show.slang")
  end

  def index
    users = User.all
    render("index.slang")
  end

  def new
    render "new.slang"
  end

  def edit
    render("edit.slang")
  end

  def create
    user = User.new user_params.validate!
    pass = user_params.validate!["password"]
    user.password = pass if pass

    if user.save
      session[:user_id] = user.id
      ScimClient.new.create_user(user)
      redirect_to "/", flash: {"success" => "Created User successfully."}
    else
      flash[:danger] = "Could not create User!"
      render "new.slang"
    end
  end

  def update
    user.set_attributes user_params.validate!
    if user.save
      ScimClient.new.update_user(user)
      redirect_to "/", flash: {"success" => "success"}
    else
      flash[:danger] = "Could not update User!"
      render "edit.slang"
    end
  end

  def destroy
    user.destroy
    ScimClient.new.delete_user(user)
    redirect_to "/", flash: {"success" => "User has been deleted."}
  end

  private def user_params
    params.validation do
      required :email
      optional :name
      optional :id
      optional :phone
      optional :password
    end
  end

  private def set_user
    if params[:id]?
      @user = User.find!(params[:id])
    else
      @user = current_user.not_nil!
    end
  end
end
