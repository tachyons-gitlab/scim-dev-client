class Setting < Granite::Base
  connection sqlite
  table settings

  column id : Int64, primary: true
  column scim_token : String?
  column scim_endpoint : String?
end
