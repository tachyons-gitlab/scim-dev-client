require "http/client"
require "uri"

class ScimClient
  @token : String
  @endpoint : String
  property :token, :endpoint

  def initialize
    @token = Setting.first!.scim_token || ""
    @endpoint = Setting.first!.scim_endpoint || ""
  end

  def create_user(user : User)
    body = {
      "externalId" => user.id.to_s,
      "active"     => true,
      "userName"   => user.name,
      "emails"     => [
        {
          "primary" => true,
          "value"   => user.email,
        },
      ],
      "name" => {
        "formatted"  => user.name,
        "familyName" => user.name,
        "givenName"  => user.name,
      },
      "schemas" => ["urn:ietf:params:scim:schemas:core:2.0:User"],
      "meta"    => {"resourceType" => "User"},
    }

    response = HTTP::Client.post(endpoint + "/Users", headers: HTTP::Headers{"User-Agent"    => "SCIM client",
                                                                             "Authorization" => "Bearer #{token}",
                                                                             "Content-Type"  => "application/scim+json",
    }, body: body.to_json)
    response
  end

  def update_user(user, attributes = [] of String)

    operations =  [{"op" => "Update", "path" => "userName", "value" => user.name}]
    operations << {"op" => "Update", "path" => "name.formatted", "value" => user.name}

    body = {
      "Operations" => operations,
    }
    response = HTTP::Client.patch(endpoint + "/Users/#{user.id}", headers: HTTP::Headers{"User-Agent"    => "SCIM client",
                                                                                         "Authorization" => "Bearer #{token}",
                                                                                         "Content-Type"  => "application/scim+json",
    }, body: body.to_json)
  end

  def delete_user(user)
    response = HTTP::Client.delete(endpoint + "/Users/#{user.id}", headers: HTTP::Headers{"User-Agent"    => "SCIM client",
                                                                                          "Authorization" => "Bearer #{token}",
                                                                                          "Content-Type"  => "application/scim+json",
    })
  end

  private def http_client
    @http_client = HTTP::Client.new(host, tls: true, port: 443)
  end
end
