FROM crystallang/crystal:1.9.2

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qq && apt-get install -y libsqlite3-dev libreadline-dev libxml2-dev 

WORKDIR /opt/amber

# Build Amber
ENV PATH /opt/amber/bin:$PATH
COPY . /opt/amber
RUN shards build amber

ENV AMBER_ENV production
WORKDIR /app

COPY shard.* /app/
RUN shards install

COPY . /app

RUN shards build scim_client --release

CMD ["./bin/scim_client"]
ENTRYPOINT ["/bin/bash", "/app/docker/entrypoint.sh"]
